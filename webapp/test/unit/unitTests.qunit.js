/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"rdrzfuffa/report_openai/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
