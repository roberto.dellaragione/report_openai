/* global QUnit */

sap.ui.require(["rdr/zfuffa/reportopenai/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
