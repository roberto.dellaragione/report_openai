sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel) {
        "use strict";

        return Controller.extend("rdr.zfuffa.reportopenai.controller.View1", {
            onInit: function () {
                this._oReportModel = new JSONModel({
                    repid: "",
                    source: "",
                    analysis: ""
                });

                this.getView().setModel(this._oReportModel, "report");
            },

            onInpRepidSubmit: function(oEvent) {
                var oMainModel = this.getView().getModel();
                var oReport = this._oReportModel.getData();

                var oKey = oMainModel.createKey("/Reports", {
                    REPID: oReport.repid
                });                

                sap.ui.core.BusyIndicator.show(0);
                oMainModel.read(oKey, {
                    success: function(oRes) {
                        sap.ui.core.BusyIndicator.hide();
                        console.log(oRes);

                        oReport.source = oRes.SOURCE;
                        this._oReportModel.setData(oReport);

                        var sConversation = "Human: What is this abap code doing?\n";
                        sConversation = sConversation + oReport.source;

                        var data = { 
                            model: "text-davinci-003",
                            prompt: sConversation,
                            max_tokens: 800,
                            temperature: 0.9,
                            top_p: 1,
                            stop: [" Human:", " AI:"]
                        }                
        
                        sap.ui.core.BusyIndicator.show(0);
                        $.ajax({
                            url: "https://api.openai.com/v1/completions",
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": "Bearer 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"
                            },
                            type: "POST",
                            dataType: "json",
                            data: JSON.stringify(data),
                            success: function(sResult) {
                                sap.ui.core.BusyIndicator.hide();
                                console.log(sResult);
                                console.log(sResult.choices[0].text);
                                oReport.analysis = sResult.choices[0].text;
                                this._oReportModel.setData(oReport);
                            }.bind(this),
        
                            error: function(error) {
                                sap.ui.core.BusyIndicator.hide();
                                console.log(error);
                            }
                        })                        
                    }.bind(this),

                    error: function(oError) {
                        sap.ui.core.BusyIndicator.hide();
                        console.log(oError);
                    }
                });
            }
        });
    });
